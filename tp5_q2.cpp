#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <limits.h>
#include <unistd.h>

#include <string>
#include "Random/CLHEP/Random/MTwistEngine.h"

int main()
{
    CLHEP::MTwistEngine * mtRng = new CLHEP::MTwistEngine();

    int i;
    int j;
    double fRn;
    unsigned int iRn;
    std::string nom_fichier_base = "q2_status";
    std::string nom_fichier = "";

    for (i = 0; i < 3; i++)
    {
        nom_fichier = nom_fichier_base + std::to_string(i);
        mtRng->saveStatus(nom_fichier.c_str());

        std::cout << "i = " << i << std::endl;
        
        for (j = 0; j < 10; j++)
        {
            fRn = mtRng->flat();    // double entre 0 et 1
            iRn = (unsigned int) (fRn * UINT_MAX);
            std::cout << iRn << std::endl;
        }

    }

    std::cout << "Séries restaurées" << std::endl;

    std::cout << "i = 1" << std::endl;

    mtRng->restoreStatus("q2_status1");

    for (j = 0; j < 10; j++)
    {
        fRn = mtRng->flat();    // double entre 0 et 1
        iRn = (unsigned int) (fRn * UINT_MAX);
        std::cout << iRn << std::endl;
    }

    return 0;
}