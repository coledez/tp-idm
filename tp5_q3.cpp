#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <limits.h>
#include <unistd.h>

#include <string>
#include "Random/CLHEP/Random/MTwistEngine.h"

int main()
{
    CLHEP::MTwistEngine * mtRng = new CLHEP::MTwistEngine();

    int i;
    int j;
    double fRn;
    unsigned int iRn;
    std::string nom_fichier_base = "q3_status";
    std::string nom_fichier = "";

    for (i = 0; i < 10; i++)
    {
        nom_fichier = nom_fichier_base + std::to_string(i);
        mtRng->saveStatus(nom_fichier.c_str());
        
        for (j = 0; j < 2000000000; j++)
        {
            fRn = mtRng->flat();    // double entre 0 et 1
        }

    }

    return 0;
}