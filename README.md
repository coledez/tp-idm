# TP IDM

Il y a un fichier différent par question ou groupe de questions indépendant. Voici comment les compiler :

```
cd Random
```

Compiler la question 2 :

```
g++ ../tp5_q2.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o tp5_q2
```

Compiler la question 3 :

```
g++ ../tp5_q3.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o tp5_q3
```

Compiler la question 4 :

```
g++ ../tp5_q4.cpp -I./include ./lib/libCLHEP-Random-2.1.0.0.a -static -o tp5_q4
```

